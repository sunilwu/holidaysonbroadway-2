<?php
/*
 * Present the slideshow
 */
?>

<div class="slideshow-container row">
    <div class="small-12 columns">

        <div class="owl-carousel">

            <div  data-slide="1" >
                <a href="#">
                    <img src="img/slides/HFTH0005.jpg"
                         alt="#"/>
                </a>
            </div>

            <div data-slide="2">
                <a href="#">
                    <img src="img/slides/HFTH0011.jpg"
                         alt="#"/>
                </a>
            </div>

            <div data-slide="3">
                <a href="#">
                    <img src="img/slides/HFTH0012.jpg"
                         alt="#"/>
                </a>
            </div>

            <div data-slide="4">
                <a href="#">
                    <img src="img/slides/HFTH0023.jpg"
                         alt="#"/>
                </a>
            </div>

            <div data-slide="5">
                <a href="#">
		    <img src="img/slides/HFTH0030.jpg"
                         alt="#"/>
                </a>
            </div>

        </div>

    </div>
</div>  <!-- ENDS .slideshow-container -->
