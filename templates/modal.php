<?php
/*
 * The modal window
 */
?>

<a id="demo01"  href="#animatedModal" style="display: none" ></a>

<!--DEMO01-->
<div id="animatedModal">
    <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->


    <div class="close-animatedModal">
        <a href="#">
        </a>
    </div>


    <div class="modal-content">

        <div class="cycle-slideshow"
             data-cycle-paused="true"
             data-cycle-slides=".modal-slide-container"
             data-cycle-prev=".nav-item.prev"
             data-cycle-next=".nav-item.next"
        >

            <div class="modal-slide-container" data-modal-slide="1">
		<div class="video-wrap">
		    <div id="player_1"></div>
		</div>
            </div>

            <div class="modal-slide-container" data-modal-slide="2">
		<div class="video-wrap">
		    <div id="player_2"><a href="">in player</a></div>
		</div>
            </div>

            <div class="modal-slide-container" data-modal-slide="3">
		<div class="video-wrap">
		    <div id="player_3"><a href="">in player</a></div>
		</div>
            </div>

            <div class="modal-slide-container" data-modal-slide="4">
		<div class="video-wrap">
		    <div id="player_4"><a href="">in player</a></div>
		</div>
            </div>

            <div class="modal-slide-container" data-modal-slide="5">
		<div class="video-wrap">
		    <div id="player_5"><a href="">in player</a></div>
		</div>
            </div>

        </div> <!-- ENDS .cycle-slideshow -->

    </div>

    <div class="slideNav">
        <div class="nav-content">

            <div class="nav-item prev"><a href="#"></a></div>
            <div class="nav-item next"><a href="#"></a></div>

        </div>
    </div>

</div>  <!-- ENDS #animatedModal -->
