<!DOCTYPE HTML>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
	<!-- DEFAULT META -->
	<title>Home for the Holidays - Live on Broadway</title>
	<meta name="description" content="For the first time ever, see the winners of American Idol, The Voice and America's Got Talent live on stage in Broadway's biggest holiday concert, hosted by The Bachelorette's Kaitlyn Bristowe." />
	<meta name="keywords" content="Home for the Holidays, Home for the Holidays Broadway, Home for the Holidays on Broadway, Home for the Holidays Concert, Home for the Holidays the Musical, Home for the Holidays Tickets, Candice Glover, Josh Kaufman, Bianca Ryan, Kailyn Bristowe, Danny Aiello, Petter Hollens, Evynne Hollens, Broadway Musical, Broadway Tickets, Home for the Holidays Discount Tickets, Home for the Holidays Live on Broadway" />
	<meta name="author" content="DR Advertising">
	
	<!-- FACBOOK META-->
	<meta property="og:title" content="Home for the Holidays - Live on Broadway" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://holidaysonbroadway.com/" />
	<meta property="og:image" content="#" />
	<meta property="og:description" content="For the first time ever, see the winners of American Idol, The Voice and America's Got Talent live on stage in Broadway's biggest holiday concert, hosted by The Bachelorette's Kaitlyn Bristowe." />
	<meta property="og:site_name" content="Home for the Holidays - Live on Broadway" />
	
	<!-- TWITTER META -->
	<meta name="twitter:card" content="product"/>
	<meta name="twitter:site" content="@HolidaysOnBway"/>
	<meta name="twitter:title" content="Home for the Holidays - Live on Broadway"/>
	<meta name="twitter:description" content="For the first time ever, see the winners of American Idol, The Voice and America's Got Talent live on stage in Broadway's biggest holiday concert, hosted by The Bachelorette's Kaitlyn Bristowe."/>
	<meta name="twitter:creator" content="@HolidaysOnBway"/>
	<meta name="twitter:image:src" content="#"/>
	
	<link rel="icon" href="img/favicon.gif">
	<link rel="stylesheet" href="css/foundation.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css" />
</head>

<body class="privacy">
	
	<section class="logo">
		<div><a href="/"><img src="img/logo-privacy.jpg" alt="Privacy Policy" style="width:100%;" /></a></div>
	</section>
	
	<section class="privacy">
		<div class="row">
			<div class="small-12 columns">
				<div class="privacy-cont">
				<h1>Privacy Policy</h1>
					<h3>What information do we collect?</h3>
					<p>We collect information from you when you register on our site, subscribe to our newsletter or fill out a form.</p>
					<p>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, physical address and/or telephone number. You may, however, visit our site anonymously.</p>
			
					<h3>What do we use your information for?</h3>
					<p>Any of the information we collect from you may be used in connection with contests, promotions, advertisements, inquiries, surveys, newsletters, e-news and/or other site features.</p>
					
					<h3>How do we protect your information?</h3>
					<p>We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.</p>
					
					<h3>Do we use cookies?</h3>
					<p>Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information).</p>
					<p>We use cookies to compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.</p>
					
					<h3>Do we disclose any information to outside parties?</h3>
					<p>We may share your information in connection with other productions, cultural institutions and/or live events as well as with third parties who assist us in operating our website, conducting our business, or servicing you. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety.</p>
					
					<h3>Third party links</h3>
					<p>Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites.</p>
					
					<h3>California Online Privacy Protection Act Compliance</h3>
					<p>We value your privacy and have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act.</p>
					
					<h3>Your Consent</h3>
					<p>By using our site, you consent to our online privacy policy.</p>
					
					<h3>Changes to our Privacy Policy</h3>
					<p>If we decide to change our privacy policy, we will post those changes on this page.</p>
				</div>
			</div>
		</div>		
	</section>
	
	<script src="js/vendor/jquery.js"></script>
  <script src="js/vendor/what-input.js"></script>
  <script src="js/vendor/foundation.min.js"></script>
	<script src="js/jquery.fittext.js"></script>
	
	<script>
		$(document).ready(function() {
			$(document).foundation();
			$(".toptext").fitText(2.0, { maxFontSize: '49px' });
		});
		
		$(window).on("orientationchange",function(){
			$(document).foundation();
			$(".toptext").fitText(2.0, { maxFontSize: '49px' });
		});
  </script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-107791536-1', 'auto');
  ga('require', 'linker');
  ga('linker:autoLink', ['ticketmaster.com'] );
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

	</script>
	
	
	<!-- Adroll -->
	<script type="text/javascript">
	    adroll_adv_id = "LAYSL4BQGNEYTHTHXKVG2U";
	    adroll_pix_id = "6IVHHWJY4FCUFJ5LERGH7O";
	    /* OPTIONAL: provide email to improve user identification */
	    /* adroll_email = "username@example.com"; */
	    (function () {
	        var _onload = function(){
	            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
	            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
	            var scr = document.createElement("script");
	            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
	            scr.setAttribute('async', 'true');
	            scr.type = "text/javascript";
	            scr.src = host + "/j/roundtrip.js";
	            ((document.getElementsByTagName('head') || [null])[0] ||
	                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
	        };
	        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
	        else {window.attachEvent('onload', _onload)}
	    }());
	</script>
	
	
	<!-- Sizmek -->
	<script type='text/javascript'>
	var ebRand = Math.random()+'';
	ebRand = ebRand * 1000000;
	//<![CDATA[ 
	document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=1094331&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
	//]]>
	</script>
	<noscript>
	<img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=1094331&amp;ns=1"/>
	</noscript>
	
	
	<!-- Google Code for Remarketing Tag -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 955372983;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/955372983/?guid=ON&amp;script=0"/>
	</div>
	</noscript>
	
	
	<!-- Facebook -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '1562367523821119');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=1562367523821119&ev=PageView&noscript=1"
	/></noscript>
</body>
</html>