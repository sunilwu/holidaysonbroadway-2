$ = jQuery.noConflict() ;


jQuery(document).ready(function() {

    // play header video as soon as possible
    document.getElementById('header-video').play() ;

    /* logic for video sound control*/
    var video = document.getElementById("header-video");
    var controls = $(".video-controls p");
    var isMuted = video.muted ;

    controls.on("click", function(e) {
        e.preventDefault();

        if (video.muted) {
            video.muted = false ;
            $(".icon-volume-off").addClass("hidden");
            $(".icon-volume-up").removeClass("hidden");
        }
        else if ( video.muted = true) {
            video.muted = true ;
            $(".icon-volume-off").removeClass("hidden");
            $(".icon-volume-up").addClass("hidden");
        }
    });


    /* carousel*/
    $(".owl-carousel").owlCarousel({
        margin: 10,
        nav: true,
        responsive : {
            0 : {
                items : 1
            },
            768 : {
                items: 3,
                margin: 20
            }
        }
    });


    /*
     *  Animated Modal
     */
    var hobModal = $("#demo01").animatedModal({
        color: '#121212',
        afterClose : function() {

        }
    });

    /*
     *   Logic for Media UI
     */
    $(".owl-item a").on("click", function(e) {
        e.preventDefault()

        /* Get slide number */
        var previewID  = $(this).parent().data('slide') ;

        /* Get the target */
        var slideTargetNumber = ( previewID -1) ;

        /* open modal and go to target slide */
        hobModal.open() ;
        $('.cycle-slideshow').cycle(slideTargetNumber);

    }) ;

    /*
     * Settings that will by used by the youtube player
     */
    playervars = {
        enablejsapi : 1,
        modestbranding : 1,
        origin : "https://www.youtube.com" ,
        showinfo : 0,
        controls : 0
    };

    /*
     * This executes automatically when the YouTube Iframe API is loaded
     */
    window.onYouTubeIframeAPIReady = function() {
        console.log("Got to onYouTubeIframeAPIReady");

        /*
         *   Single function to make our youtube API calls consistent
         */
        window.swVideosLoaded= [] ;
        window.players = [] ;
        function swGetYoutubeVids ( elById, videoId ) {
            currentNum = window.players.length ;
            newNum = currentNum + 1 ;

            /* actually create the player */
            Player = new YT.Player( elById, {
                height : '',
                width: '',
                videoId : videoId,
                playerVars: playervars,
                events :  {
                }
            } ) ;
            window.players.push(Player) ;
        }

        // now actually get the videos
        swGetYoutubeVids('player_1', 'RryNwynmG6k' ) ;
        swGetYoutubeVids('player_2', 'RY-CNTLXmMI' ) ;
        swGetYoutubeVids('player_3', 'B-_KvoMR4sU') ;
        swGetYoutubeVids('player_4', 'ha9OFDUunMU' ) ;
        swGetYoutubeVids('player_5', 'zDnwmtyCkIE' ) ;

    };

    /*
     * Get the YouTube Iframe API
     */
    $.getScript("https://www.youtube.com/iframe_api")
        .done(function() {
            console.log("We loaded the script successfully");
        })
        .fail(function() {
            console.log("Something went wrong when we tried to get and load the youtube iframe API");
        });


    function stopAllVideos() {
        window.players.forEach( function(player) {
            player.stopVideo() ;
        }) ;
    }

    /*
     *  Stop videos playing when we go to next slide
     */
    function stopAllVideos() {
        window.players.forEach( function(player) {
            player.stopVideo() ;
        }) ;
    }


    $(".nav-item.next, .nav-item.prev, .close-animatedModal a").on("click", function() {
	console.log("Clicked on the button");
        stopAllVideos() ;
    }) ;

});
